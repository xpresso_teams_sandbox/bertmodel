"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import json
import pandas as pd
import torch
import transformers
from torch.utils.data import Dataset, DataLoader
from transformers import BertTokenizer, BertModel, BertConfig

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="model",
                   level=logging.INFO)

class Data(Dataset):
    def __init__(self, dataframe, tokenizer, max_len):
        self.len = len(dataframe)
        self.data = dataframe
        self.tokenizer = tokenizer
        self.max_len = max_len

    def __getitem__(self, index):
        title = str(self.data.TITLE[index])
        title = " ".join(title.split())
        inputs = self.tokenizer.encode_plus(
            title,
            None,
            add_special_tokens=True,
            max_length=self.max_len,
            pad_to_max_length=True,
            return_token_type_ids=True
        )
        ids = inputs['input_ids']
        mask = inputs['attention_mask']

        return {
            'ids': torch.tensor(ids, dtype=torch.long),
            'mask': torch.tensor(mask, dtype=torch.long),
            'targets': torch.tensor(self.data.ENCODE_CAT[index], dtype=torch.long)
        }

    def __len__(self):
        return self.len

# Class for BERT model that takes in the number of labels and model name
class BERTClass(torch.nn.Module):
    def __init__(self, numLabels, model_name):
        super(BERTClass, self).__init__()
        self.l1 = BertModel.from_pretrained(model_name)
        self.classifier = torch.nn.Linear(768, numLabels) #where does 768 come from

    def forward(self, input_ids, attention_mask):
        output_1 = self.l1(input_ids=input_ids, attention_mask=attention_mask)
        hidden_state = output_1[0]
        pooler = hidden_state[:, 0]
        output = self.classifier(pooler)
        return output


class Model(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="Model")
        """ Initialize all the required constants and data here """

    def start(self, run_name, labels):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            data = self.getConfigOptions("config.json")
            print(data["model_name"][ 1])
            model_name = data["model_name"][1] #change based on what model you want
            MAX_LEN = data["max_len"]
            TRAIN_BATCH_SIZE = data["train_batch_size"]
            VALID_BATCH_SIZE = data["valid_batch_size"]
            EPOCHS = data["epochs"]
            LEARNING_RATE = data["learning_rate"]
            config = BertConfig(data)
            tokenizer = BertTokenizer.from_pretrained(model_name)
            device = 'cuda'
            train_dataset = pd.read_csv("/data/dataset_train.csv", sep='\t', names=labels)
            test_dataset = pd.read_csv("/data/dataset_test.csv", sep='\t', names=labels)


            training_set = Data(train_dataset, tokenizer, MAX_LEN)
            testing_set = Data(test_dataset, tokenizer, MAX_LEN)

            train_params = {'batch_size': TRAIN_BATCH_SIZE,
                            'shuffle': True,
                            'num_workers': 0
                            }

            test_params = {'batch_size': VALID_BATCH_SIZE,
                            'shuffle': True,
                            'num_workers': 0
                            }

            training_loader = DataLoader(training_set, **train_params)
            testing_loader = DataLoader(testing_set, **test_params)


            model = BERTClass(6, model_name)
            model.to(device)
            optimizer = torch.optim.Adam(params =  model.parameters(), lr=LEARNING_RATE)



            for epoch in range(EPOCHS):
                self.train(epoch, training_loader, model, device, optimizer)


            print('This is the validation section to print the accuracy and see how it performs')
            print('Here we are leveraging on the dataloader crearted for the validation dataset, the approcah is using more of pytorch')

            acc = self.valid(model, testing_loader, device)

            print("Accuracy on test data = %0.2f%%" % acc)


            ### $xpr_param_pipeline_job_import

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        # SAVE MODEL CODE

        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def getConfigOptions(self, filename):
        with open(filename, "r") as read_file:
            data = json.load(read_file)
        return data
    def calcuate_accu(self, big_idx, targets):
        n_correct = (big_idx==targets).sum().item()
        return n_correct

    # loss fuction for use in train
    loss_function = torch.nn.CrossEntropyLoss()

    def train(self, epoch, training_loader, model, device, optimizer):
        tr_loss = 0
        n_correct = 0
        nb_tr_steps = 0
        nb_tr_examples = 0
        model.train()
        for _,data in enumerate(training_loader, 0):
            ids = data['ids'].to(device, dtype = torch.long)
            mask = data['mask'].to(device, dtype = torch.long)
            targets = data['targets'].to(device, dtype = torch.long)

            outputs = model(ids, mask)
            loss = self.loss_function(outputs, targets)
            tr_loss += loss.item()
            big_val, big_idx = torch.max(outputs.data, dim=1)
            n_correct += self.calcuate_accu(big_idx, targets)

            nb_tr_steps += 1
            nb_tr_examples+=targets.size(0)

            if _%5000==0:
                loss_step = tr_loss/nb_tr_steps
                accu_step = (n_correct*100)/nb_tr_examples
                print(f"Training Loss per 5000 steps: {loss_step}")
                print(f"Training Accuracy per 5000 steps: {accu_step}")

            optimizer.zero_grad()
            loss.backward()
            # When using GPU
            optimizer.step()
        print(f'The Total Accuracy for Epoch {epoch}: {(n_correct*100)/nb_tr_examples}')
        epoch_loss = tr_loss/nb_tr_steps
        epoch_accu = (n_correct*100)/nb_tr_examples
        print(f"Training Loss Epoch: {epoch_loss}")
        print(f"Training Accuracy Epoch: {epoch_accu}")


        return


    def valid(self, model, testing_loader, device):
        model.eval()
        n_correct = 0; n_wrong = 0; total = 0; tr_loss = 0
        with torch.no_grad():
            for _, data in enumerate(testing_loader, 0):
                ids = data['ids'].to(device, dtype = torch.long)
                mask = data['mask'].to(device, dtype = torch.long)
                targets = data['targets'].to(device, dtype = torch.long)
                outputs = model(ids, mask).squeeze()
                loss = self.loss_function(outputs, targets)
                tr_loss += loss.item()
                big_val, big_idx = torch.max(outputs.data, dim=1)
                n_correct += self.calcuate_accu(big_idx, targets)

                nb_tr_steps += 1
                nb_tr_examples+=targets.size(0)

                if _%5000==0:
                    loss_step = tr_loss/nb_tr_steps
                    accu_step = (n_correct*100)/nb_tr_examples
                    print(f"Validation Loss per 100 steps: {loss_step}")
                    print(f"Validation Accuracy per 100 steps: {accu_step}")
        epoch_loss = tr_loss/nb_tr_steps
        epoch_accu = (n_correct*100)/nb_tr_examples
        print(f"Validation Loss Epoch: {epoch_loss}")
        print(f"Validation Accuracy Epoch: {epoch_accu}")

        return epoch_accu



if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = Model()
    labels = ['ID','TITLE', 'URL', 'PUBLISHER', 'CATEGORY', 'STORY', 'HOSTNAME', 'TIMESTAMP']
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1], labels)
    else:
        data_prep.start(run_name="")
